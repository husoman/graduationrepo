﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script makes bullet trail move
public class MoveTrail : MonoBehaviour
{
	int moveSpeed;

	void Start ()
	{
		moveSpeed = 50;
	}

	void Update ()
	{
		// Claimed that using Translate is better than using Rigidbody2D (Because it is light-weight)
		// Thanks to deltaTime, not affected by frame rate
		transform.Translate (Vector3.right * Time.deltaTime * moveSpeed);
		// Self destruction with time
		Destroy (gameObject, 1);
	}

	void OnTriggerEnter2D (Collider2D coll)
	{
		Destroy (gameObject);
	}
}
