﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// It deals with new screen operations
// It deals with the tutorial
// It deals with restarting levels
public class WorldController : MonoBehaviour
{
	Camera cam;
	Color originalColor;
	public AudioSource funbgm;
	public Transform cameraTransform;
	public GameObject tutorial, got, arrow;
	public GameObject grass2;
	public GameObject okayButton, characterLife;
	public float height, width;
	int levelCounter;
	public bool dragCompleted, pause, characterDead;
	public float indexValueX = 8.58f;
	public float indexValueY = 2f;
	public bool draggingTime;

	void Start()
	{
		Time.timeScale = 1f;
		dragCompleted = true;	// Let the character move at the beginning
		pause = false;
		characterDead = false;
		arrow.SetActive(false);
		draggingTime = false;
		characterLife.SetActive(false);
		okayButton.SetActive(false);
		originalColor = okayButton.GetComponent<Image>().color;
		cam = Camera.main;
		height = 2f * cam.orthographicSize;
		width = height * cam.aspect;
		levelCounter = 0;
		destroySizers();

		// Adding Channels 
		CommunicationController commController = CommunicationController.getInstance();
		Channel turtleChannel = commController.createChannel(Channel.Type.TURTLE);
		Channel birdChannel = commController.createChannel(Channel.Type.BIRD);
		Channel greenDwarfChannel = commController.createChannel(Channel.Type.GREENDWARF);
		// Adding Subscribers
		foreach (GameObject turtle in GameObject.FindGameObjectsWithTag("Turtle")) {
			turtleChannel.addSubscriber(turtle.GetComponent<Turtle>());
		}
		foreach (GameObject bird in GameObject.FindGameObjectsWithTag("Bird")) {
			birdChannel.addSubscriber(bird.GetComponent<Bird>());
		}
		foreach (GameObject dwarf in GameObject.FindGameObjectsWithTag("Green Dwarf")) {
			greenDwarfChannel.addSubscriber(dwarf.GetComponent<GreenDwarf>());
		}

		// Music control
		if (PlayerPrefs.GetInt("music") == 1) {
			funbgm.Play();
		}
	}

	void Update()
	{
		// When we passed to next level
		if (FindObjectOfType<ManController>().rb.position.x - cameraTransform.position.x > width / 2) {
			cameraTransform.position = new Vector3(cameraTransform.position.x + width, cameraTransform.position.y, cameraTransform.position.z);
			FindObjectOfType<LevelController>().PrepareLevel(levelCounter);	// prepare next level
			levelCounter++;
		}
		// When we die
		if (characterDead == true) {
			FindObjectOfType<ManController>().transform.position = new Vector2(cameraTransform.position.x - indexValueX, cameraTransform.position.y - indexValueY);
			FindObjectOfType<ManController>().remainingHealth--;
			if (FindObjectOfType<ManController>().remainingHealth == 0) {
				// start from scratch
				SceneManager.LoadScene("First");
			}
			if (levelCounter - 1 == 0) {
				// restart tutorial level
				FindObjectOfType<LevelController>().PrepareLevel(-1);
			} else {
				// restart current level
				FindObjectOfType<LevelController>().PrepareLevel(levelCounter - 1);
			}
			if(PlayerPrefs.GetInt("music") == 1){
				funbgm.Play();	
			}
			characterDead = false;
		}
	}



	// After tutorial, handle first level starting operations
	public void GotIt()
	{
		// Clean the screen
		FindObjectOfType<MenuController>().filter.SetActive(false);
		Destroy(tutorial.gameObject);
		got.SetActive(false);
		// Get animation back to its normal routine
		FindObjectOfType<ManController>().anim.speed = 1f;
		pause = false;
		dragCompleted = false;
		FindObjectOfType<MenuController>().pauseButton.SetActive(true);
		grass2.transform.localPosition = new Vector2(0f, 0f);
		activateDragFunctionality();
		arrow.SetActive(true);
	}

	// When pause button is clicked, run this function
	public void Pause()
	{
		funbgm.Pause();
		pause = true;
		Time.timeScale = 0f;
		okayButton.SetActive(false);
		characterLife.SetActive(false);
		arrow.SetActive(false);
		deactivateDragFunctionality();
	}

	// Reverse of pause
	public void Continue()
	{
		if(PlayerPrefs.GetInt("music") == 1){
			funbgm.Play();	
		}
		pause = false;
		Time.timeScale = 1f;
		if (dragCompleted == false) {
			activateDragFunctionality();
			okayButton.SetActive(true);
		}
		if (levelCounter > 1) {
			characterLife.SetActive(true);
		}
	}

	// When the OK button is pressed
	public void okayOperations()
	{
		if (draggingTime == false) {
			okayButton.SetActive(false);
			// Unfreeze the game when the player presses the OK button
			dragCompleted = true;
			deactivateDragFunctionality();
			stopYellow();
			arrow.SetActive(false);
		}
	}

	public void activateDragFunctionality()
	{
		var grasses = GameObject.FindGameObjectsWithTag("Grass");
		foreach (GameObject item in grasses) {
			item.GetComponent<Drag>().activation = true;
		}
	}

	void deactivateDragFunctionality()
	{
		var grasses = GameObject.FindGameObjectsWithTag("Grass");
		foreach (GameObject item in grasses) {
			item.GetComponent<Drag>().activation = false;
		}
	}

	// Cleans unnecessary objects for better performance
	void destroySizers()
	{
		var sizers = GameObject.FindGameObjectsWithTag("Sizer");
		foreach (GameObject item in sizers) {
			Destroy(item);
		}
	}
		
	//  Make Okay Button active and make it look active
	public void activateOkayButton()
	{
		draggingTime = false;
		okayButton.GetComponent<Image>().color = originalColor;
	}

	// Make Okay Button inactive and make it look inactive
	public void inactivateOkayButton()
	{
		draggingTime = true;
		okayButton.GetComponent<Image>().color = Color.gray;
	}

	public void startYellow()
	{
		var dragParticles = GameObject.FindGameObjectsWithTag("Drag Particles");
		foreach (GameObject item in dragParticles) {
			//item.GetComponent<ParticleSystem> ().enableEmission = true;
			ParticleSystem ps = item.GetComponent<ParticleSystem>();
			var em = ps.emission;
			em.enabled = true;
		}
	}

	void stopYellow()
	{
		var dragParticles = GameObject.FindGameObjectsWithTag("Drag Particles");
		foreach (GameObject item in dragParticles) {
			//item.GetComponent<ParticleSystem> ().enableEmission = false;
			ParticleSystem ps = item.GetComponent<ParticleSystem>();
			var em = ps.emission;
			em.enabled = false;
		}
	}

	public void destroyUfos()
	{
		var ufos = GameObject.FindGameObjectsWithTag("Ufo");
		foreach (GameObject item in ufos) {
			Destroy(item);
		}
	}
}