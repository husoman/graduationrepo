﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script applies the magnetic attraction
public class MagneticAttraction : MonoBehaviour
{
	public bool magnetize = false;
	public GameObject arm, cursor;
	float attractionDistanceMan = 7f;
	float attractionDistanceGun = 15f;
	public GameObject[] targetObjects;
	GameObject man;
	float distance;
	float step = 7f;

	void Start ()
	{
		man = FindObjectOfType<ManController> ().gameObject;
	}

	void Update ()
	{
		distance = Vector2.Distance (man.transform.position, transform.position);
		// Activation conditions of magnetism
		if (distance <= attractionDistanceMan) {
			FindObjectOfType<ManController> ().gunActive = false;
			// Make the attractable item a child of the magnet so that it will move with the magnet
			var simpleGuns = GameObject.FindGameObjectsWithTag ("Simple Gun");
			foreach (GameObject item in simpleGuns) {
				if (Vector2.Distance (item.transform.position, transform.position) <= attractionDistanceGun) {
					item.transform.parent = transform;
				}
			}
			var automaticGuns = GameObject.FindGameObjectsWithTag ("Automatic Gun");
			foreach (GameObject item in automaticGuns) {
				if (Vector2.Distance (item.transform.position, transform.position) <= attractionDistanceGun) {
					item.transform.parent = transform;
				}
			}
			// Attract the target objects
			foreach (Transform child in transform) {
				//Debug.Log (child.tag);
				arm.SetActive (false);
				cursor.SetActive (false);
				child.transform.position = Vector2.MoveTowards (child.transform.position, transform.position, Time.deltaTime * step);
				Debug.Log("Movin toward");
				UnityEngine.Cursor.visible = true;
			}
		}
	}
}
