﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script lets us drag and drop platforms
public class Drag : MonoBehaviour
{
	Vector3 distance;
	float positionX;
	float positionY;
	Vector3 currentPosition;
	Vector3 worldPosition;
	public bool activation;

	void Start ()
	{
		activation = false;
	}

	void OnMouseDown ()
	{
		// Take the position of the object (which this script is attached on),
		// and transfer its coordinates from world space to screen space
		distance = Camera.main.WorldToScreenPoint (transform.position);
		positionX = Input.mousePosition.x - distance.x;
		positionY = Input.mousePosition.y - distance.y;
	}

	// Called every frame while the mouse is down
	void OnMouseDrag ()
	{
		if (activation == true) {
			FindObjectOfType<WorldController> ().activateOkayButton ();
			currentPosition = new Vector3 (Input.mousePosition.x - positionX, Input.mousePosition.y - positionY, distance.z);
			// Transfer the coordinates of the currentPosition from screen space to world space
			worldPosition = Camera.main.ScreenToWorldPoint (currentPosition);
			// Update the position of the object
			transform.position = worldPosition;
		}
	}
}
