﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script makes the cursor image follows the mouse cursor all the time
// and makes the cursor image invisible when platform drag-drop is active
public class Cursor : MonoBehaviour
{
	SpriteRenderer spriteRenderer;

	void Start ()
	{
		spriteRenderer = gameObject.GetComponent<SpriteRenderer> ();
	}

	void Update ()
	{
		transform.position = new Vector2 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x, Camera.main.ScreenToWorldPoint (Input.mousePosition).y);
		if (FindObjectOfType<WorldController> ().dragCompleted == false || FindObjectOfType<WorldController> ().pause == true) {
			spriteRenderer.enabled = false;
			UnityEngine.Cursor.visible = true;
		} else {
			spriteRenderer.enabled = true;
			UnityEngine.Cursor.visible = false;
		}
	}
}