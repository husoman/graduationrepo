﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script controls the speed of the chain
public class ChainSwing : MonoBehaviour
{
	JointMotor2D motor;
	HingeJoint2D hinge;
	float direction;
	float maximumAngle = 0.15f;
	float minimumAngle = -0.15f;
	float maximumSpeed = 15f;
	float frictionCoefficient = 20f;

	void Start ()
	{
		hinge = GetComponent<HingeJoint2D> ();
		motor = hinge.motor;
		motor.motorSpeed = maximumSpeed;
		hinge.motor = motor;
	}

	void Update ()
	{
		if (transform.rotation.z < minimumAngle) {
			maximumSpeed = -15f;
		} else if (transform.rotation.z > maximumAngle) {
			maximumSpeed = 15f;
		}
		motor.motorSpeed = maximumSpeed + (transform.rotation.z * frictionCoefficient);
		hinge.motor = motor;
	}
}
