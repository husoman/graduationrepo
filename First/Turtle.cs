﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Control the turtle behaviour
public class Turtle : MonoBehaviour, CommSubscriber
{
	public Transform turtleEye, sightLimit;
	public bool dangerSeen = false;
	public LayerMask whatIsDanger;
	float speed = 1f;
	float maximumDistance = 1.5f;
	Animator anim;
	int maximumCalmness = 300;
	int calmnessCoefficient;
	bool touched = false;
	bool hit = false;

	public enum State
	{
		WALKING,
		HIDING
	}

	public State ActiveState = State.WALKING;

	void Start ()
	{
		anim = GetComponent<Animator> ();
		calmnessCoefficient = maximumCalmness;
	}


	void Update ()
	{
		switch (ActiveState) {
		case State.WALKING:
			// Walking operations
			anim.SetBool ("Disturbed", false);
			//Debug.DrawLine (turtleEye.position, sightLimit.position, Color.red);
			dangerSeen = Physics2D.Linecast (turtleEye.position, sightLimit.position, whatIsDanger);
			transform.position = new Vector2 (transform.position.x + (Time.deltaTime * speed), transform.position.y);
			if (transform.localPosition.x > maximumDistance) {
				WalkToLeft ();
			} else if (transform.localPosition.x < -1f * maximumDistance) {
				WalkToRight ();
			}
			if (hit) {
				Destroy (gameObject);
			}
			// State changing condition
			if (dangerSeen || touched) {
				//hide ();	// Unnecessary because this turtle also receives hide message
				sendHideMessage ();
			}
			break;
		case State.HIDING:
			// Hiding operations
			anim.SetBool ("Disturbed", true);
			calmnessCoefficient++;
			if (touched || hit) {
				calmnessCoefficient = 0;
				touched = false;
				hit = false;
			}
			// State changing condition
			if (calmnessCoefficient >= maximumCalmness) {
				ActiveState = State.WALKING;
			}
			break;
		default:
			Debug.Log ("Turtle: Unknown state");
			break;
		}
	}

	void WalkToLeft ()
	{
		Vector3 scale = transform.localScale;
		scale.x = -2;
		transform.localScale = scale;
		speed = -1f;
	}

	void WalkToRight ()
	{
		Vector3 scale = transform.localScale;
		scale.x = 2;
		transform.localScale = scale;
		speed = 1f;
	}

	void OnCollisionEnter2D (Collision2D coll)
	{
		if (coll.gameObject.tag == "Man") {
			touched = true;
		} else if (coll.gameObject.tag == "Laser") {
			hit = true;
		}
	}

	// Method for turtle to get message
	public void receiveMessage (Message message)
	{
		switch (message.getContent ()) {
		case TurtleMessage.MessageContent.TURTLE_HIDE:
			hide ();
			break;
		default:
			Debug.Log ("ERROR: Message Content cannot be taken.");
			break;
		}
	}

	// Method for sending message
	private void sendHideMessage ()
	{
		TurtleMessage message = new TurtleMessage (Message.MessageContent.TURTLE_HIDE);
		CommunicationController.getInstance ().sendMessage (message, Channel.Type.TURTLE);	// Publisher gereksiz
		// Publisher olması garanti olması gereken bir durumda kullanılabilir
	}

	private void hide ()
	{
		ActiveState = State.HIDING;
		calmnessCoefficient = 0;
	}
}
