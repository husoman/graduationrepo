﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Makes move left-right
public class MoveLeftRight : MonoBehaviour
{

	public int movementRange = 200;
	int counter;	// to measure the time
	bool isDraggable = false;
	bool beingDragged = false;

	void Start()
	{
		counter = movementRange / 2;	// center of the movement
	}

	void FixedUpdate()
	{
		if (counter >= movementRange) {
			counter = -movementRange;
		}
		if (beingDragged == false) {
			// Move right
			if (counter > 0) {
				transform.position = new Vector2(transform.position.x + Time.deltaTime, transform.position.y);
			}
			// Move left
			else {
				transform.position = new Vector2(transform.position.x - Time.deltaTime, transform.position.y);
			}
			counter++;
		}
	}

	void Update()
	{
		Drag script = gameObject.GetComponent<Drag>();
		if (script != null) {
			isDraggable = true;
		}
	}

	void OnMouseDown()
	{
		if (isDraggable == true) {
			beingDragged = true;
		}
	}

	void OnMouseUp()
	{
		if (isDraggable == true) {
			beingDragged = false;
		}
	}
}
