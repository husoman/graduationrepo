﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script deals with shooting
public class Weapon : MonoBehaviour
{
	public float fireRate;
	public LayerMask whatToHit;
	float timeToFire = 0f;
	float timeToEffect = 0f;
	float effectRate = 10f;
	// Changes according to gun type
	Transform firePoint;
	public Transform bulletTrail;
	public AudioSource laserSound;

	void Start()
	{
		firePoint = transform.FindChild("FirePoint");
		if (firePoint == null) {
			Debug.LogError("No fire point");
		}
	}

	void Update()
	{
		if (FindObjectOfType<WorldController>().dragCompleted == true && FindObjectOfType<WorldController>().pause == false) {
			if (fireRate == 0f) {
				if (Input.GetButtonDown("Fire1")) {
					Shoot();
				}
			} else {
				if (Input.GetButton("Fire1") && Time.time > timeToFire) {
					timeToFire = Time.time + (1 / fireRate);
					Shoot();
				}	
			}	
		}
	}

	void Shoot()
	{
		Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
		Vector2 firePointPosition = new Vector2(firePoint.position.x, firePoint.position.y);
		Physics2D.Raycast(firePointPosition, mousePosition - firePointPosition, 100f, whatToHit);
		if (Time.time >= timeToEffect) {
			Effect();
			timeToEffect = Time.time + 1 / effectRate;
		}
	}

	void Effect()
	{
		if (PlayerPrefs.GetInt("sound") == 1) {
			laserSound.Play();
		}
		Instantiate(bulletTrail, firePoint.position, firePoint.rotation);
	}
}
