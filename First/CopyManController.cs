﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Controls the behaviours of the Copy Man
public class CopyManController : MonoBehaviour
{

	float maxSpeed, move, cameraMinusManX, symmetricalPositionX;
	public Animator anim;
	public Rigidbody2D rb;
	Vector3 scale;
	bool syncComplete;

	void Start ()
	{
		maxSpeed = 6f;
		anim = GetComponent<Animator> ();
		rb = GetComponent<Rigidbody2D> ();
		syncComplete = false;
		StartCoroutine (SyncCharacters ());
	}

	void FixedUpdate ()
	{
		if (FindObjectOfType<WorldController> ().pause == false && FindObjectOfType<WorldController> ().dragCompleted == true) {
			move = Input.GetAxis ("Horizontal");
			anim.SetFloat ("Speed", Mathf.Abs (move));
			rb.velocity = new Vector2 (-1 * move * maxSpeed, rb.velocity.y);
		}
	}

	void Update ()
	{
		// Syncing the positions for a few frames
		if (syncComplete == false) {
			cameraMinusManX = FindObjectOfType<WorldController> ().cameraTransform.position.x - FindObjectOfType<ManController> ().transform.position.x;
			symmetricalPositionX = (cameraMinusManX * 2f) + FindObjectOfType<ManController> ().transform.position.x;
			transform.position = new Vector2 (symmetricalPositionX, FindObjectOfType<ManController> ().transform.position.y);	
		}
		// Orientation is always reverse of the main character
		scale = transform.localScale;
		scale.x = FindObjectOfType<ManController> ().transform.localScale.x;
		transform.localScale = scale;
		// Jump
		if (FindObjectOfType<ManController> ().grounded && (Input.GetKeyDown (KeyCode.W) || Input.GetKeyDown (KeyCode.UpArrow))) {
			if (FindObjectOfType<WorldController> ().pause == false && FindObjectOfType<WorldController> ().dragCompleted == true) {
				transform.parent = null;
				rb.AddForce (new Vector2 (0f, 80f));
			}
		}
		// When we fall
		if (FindObjectOfType<WorldController> ().cameraTransform.position.y - transform.position.y > 10f) {
			FindObjectOfType<WorldController> ().characterDead = true;
		}
	}

	void OnCollisionEnter2D (Collision2D coll)
	{
		if (coll.gameObject.tag == "MovingPlatform") {
			transform.parent = coll.transform;
		}
		if (coll.gameObject.tag == "Laser") {
			FindObjectOfType<WorldController> ().characterDead = true;
		}
	}

	IEnumerator SyncCharacters ()
	{
		yield return new WaitForSeconds (1f);
		syncComplete = true;
	}
}
