﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Controls Green-dwarf behaviours
public class GreenDwarf : MonoBehaviour, CommSubscriber
{
	public Transform eye, sightLimit, backFocusLimit, shootingDistance, firePoint, bulletTrail;
	bool dangerSeen = false;
	bool dangerFelt = false;
	bool touched = false;
	bool shootingDecided = false;
	public LayerMask whatIsDanger;
	float speed = -0.5f;
	float closeEnough = 0.1f;
	int suspicionLevel = 0;
	int maximumSuspicion = 300;
	float patrollingDistance = 1.5f;
	public GameObject arm, gun, questionMark, warningMark;
	Vector2 target;
	float step = 1f;
	public LayerMask whatToHit;
	public AudioSource gunSound;
	float timeToEffect = 0f;
	float timeToFire = 0f;
	float effectRate = 10f;	// delay of effect is short
	float fireRate = 1f;	// time between two shoots is long

	public enum State
	{
		PATROLLING,
		ALERTED,
		SUSPICIOUS
	}

	public State ActiveState = State.PATROLLING;

	void Start ()
	{
		StartPatrolling ();
	}

	void Update ()
	{
		switch (ActiveState) {
		case State.PATROLLING:
			// Patrolling operations
			Patrol();
			// State changing condition
			if (dangerSeen || touched) {
				alertAllUnits ();
				//StartAlerted ();
			}
			break;
		case State.ALERTED:
			// Alerted operations
			transform.position = new Vector2 (Mathf.MoveTowards (transform.position.x, target.x, Time.deltaTime * step), transform.position.y);
			if (dangerSeen || dangerFelt || touched) {
				touched = false;	// return to default value
				target = FindObjectOfType<ManController> ().transform.position;
				FixFacingDirection ();
			}
			if(shootingDecided){
				if (Time.time > timeToFire) {
					timeToFire = Time.time + (1 / fireRate);
					Shoot ();
				}
			}
			// State changing condition
			if(ArrivedTargetPoint()){
				StartSuspicious ();
			}

			break;
		case State.SUSPICIOUS:
			// Suspicious operations
			suspicionLevel--;
			Patrol ();
			// State changing conditions
			if (suspicionLevel <= 0f) {
				StartPatrolling ();
			}
			if (dangerSeen || dangerFelt || touched) {
				alertAllUnits ();
				//StartAlerted ();
			}
			break;
		default:
			Debug.Log ("GreenDwarf: Unknown state");
			break;
		}
		// Assignments for all cases
		dangerSeen = Physics2D.Linecast (eye.position, sightLimit.position, whatIsDanger);
		dangerFelt = Physics2D.Linecast (eye.position, backFocusLimit.position, whatIsDanger);
		shootingDecided = Physics2D.Linecast (eye.position, shootingDistance.position, whatIsDanger);
		if (arm.activeSelf == true) {
			// Difference between the target and the fire point position
			Vector3 difference = sightLimit.position - firePoint.position;
			difference.Normalize ();
			// Find the angle in degrees
			float rotationZ = Mathf.Atan2 (difference.y, difference.x) * Mathf.Rad2Deg;
			arm.transform.rotation = Quaternion.Euler (0f, 0f, rotationZ);
		}
	}

	void Shoot(){
		Physics2D.Raycast (firePoint.position, sightLimit.position, 100f, whatToHit);
		//Debug.DrawLine(firePoint.position, sightLimit.position, Color.red, 1f);
		if (Time.time >= timeToEffect) {
			Effect ();
			timeToEffect = Time.time + 1 / effectRate;
		}
	}

	void Effect(){
		if(PlayerPrefs.GetInt("sound") == 1){
			gunSound.Play ();	
		}
		// arm'ın rotasyonu kullanılacak
		Instantiate (bulletTrail, firePoint.position, firePoint.rotation);
	}

	bool ArrivedTargetPoint(){
		bool isArrived = false;
		if (target.x < transform.position.x) {	// if target is on the left side
			if (transform.position.x - target.x <= closeEnough) {
				isArrived = true;
			}
		} else {
			if (target.x - transform.position.x <= closeEnough) {	// if target is on the right side
				isArrived = true;
			}
		}
		return isArrived;
	}

	void FixFacingDirection ()
	{
		if (transform.position.x > target.x) {
			LookToLeft ();
		} else {
			LookToRight ();
		}
	}

	void StartPatrolling ()
	{
		ActiveState = State.PATROLLING;
		arm.SetActive (false);
		gun.SetActive (false);
		questionMark.SetActive (false);
		warningMark.SetActive (false);
	}

	// Method for turtle to get message
	public void receiveMessage (Message message)
	{
		switch (message.getContent ()) {
		case GreenDwarfMessage.MessageContent.ALERT_ALL_UNITS:
			StartAlerted ();
			break;
		default:
			Debug.Log ("ERROR: Message Content cannot be taken.");
			break;
		}
	}

	// Method for sending message
	private void alertAllUnits ()
	{
		GreenDwarfMessage message = new GreenDwarfMessage (Message.MessageContent.ALERT_ALL_UNITS);
		CommunicationController.getInstance ().sendMessage (message, Channel.Type.GREENDWARF);
	}

	void StartAlerted ()
	{
		ActiveState = State.ALERTED;
		arm.SetActive (true);
		gun.SetActive (true);
		questionMark.SetActive (false);
		warningMark.SetActive (true);
	}

	void StartSuspicious ()
	{
		ActiveState = State.SUSPICIOUS;
		arm.SetActive (true);
		gun.SetActive (true);
		questionMark.SetActive (true);
		warningMark.SetActive (false);
		suspicionLevel = maximumSuspicion;
	}

	void Patrol ()
	{
		if (transform.localPosition.x > patrollingDistance) {
			PatrolLeft ();
		} else if (transform.localPosition.x < -1f * patrollingDistance) {
			PatrolRight ();
		}
		transform.position = new Vector2 (transform.position.x + (Time.deltaTime * speed), transform.position.y);
	}

	void LookToLeft ()
	{
		Vector3 scale = transform.localScale;
		scale.x = -1f;
		transform.localScale = scale;
		FixQuestionScale ();
	}

	void LookToRight ()
	{
		Vector3 scale = transform.localScale;
		scale.x = 1f;
		transform.localScale = scale;
		FixQuestionScale ();
	}

	void PatrolLeft ()
	{
		LookToLeft ();
		speed = -0.5f;
	}

	void PatrolRight ()
	{
		LookToRight ();
		speed = 0.5f;
	}

	void FixQuestionScale ()
	{
		Vector3 scale = questionMark.transform.localScale;
		if (transform.localScale.x == -1f) {
			scale.x = -1f;	
		} else {
			scale.x = 1f;
		}
		questionMark.transform.localScale = scale;
	}

	void OnCollisionEnter2D (Collision2D coll)
	{
		if (coll.gameObject.tag == "Man") {
			touched = true;
		}
	}
}
