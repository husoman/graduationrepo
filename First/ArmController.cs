﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script makes the arm stay in shoulder position
// and makes the arm rotate accordingly mouse cursor
public class ArmController : MonoBehaviour
{
	Transform manTransform;
	Vector3 difference, scale;
	float rotationZ;
	bool facingRight;
	float distanceX = 0.0505f;
	float distanceY = 0.048f;

	void Start ()
	{
		manTransform = FindObjectOfType<ManController> ().transform;
		facingRight = true;
	}

	void Update ()
	{
		transform.position = new Vector2 (manTransform.position.x + distanceX, manTransform.position.y + distanceY);
		if (FindObjectOfType<WorldController> ().dragCompleted && !FindObjectOfType<WorldController> ().pause) {
			// Difference between the mouse and the arm position
			difference = Camera.main.ScreenToWorldPoint (Input.mousePosition) - transform.position;
			difference.Normalize ();
			// Find the angle in degrees
			rotationZ = Mathf.Atan2 (difference.y, difference.x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.Euler (0f, 0f, rotationZ);
			if (difference.x > 0f && !facingRight) {
				Flip ();
			} else if (difference.x < 0f && facingRight) {
				Flip ();
			}
		}
	}

	void Flip ()
	{
		facingRight = !facingRight;
		scale = transform.localScale;
		scale.y *= -1;
		transform.localScale = scale;
	}
}
