﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

	public GameObject homeButton, playButton, pauseButton, filter;
	public GameObject soundOnButton, soundOffButton;
	public GameObject musicOnButton, musicOffButton;

	void Start(){
		RemoveMenuObjects();
	}

	public void ReturnToMainMenu(){
		SceneManager.LoadScene("Menu");
	}

	public void MakeSoundOn(){
		soundOffButton.SetActive(false);
		soundOnButton.SetActive(true);
		PlayerPrefs.SetInt("sound", 1);
	}

	public void MakeSoundOff(){
		soundOnButton.SetActive(false);
		soundOffButton.SetActive(true);
		PlayerPrefs.SetInt("sound", 0);
	}

	public void MakeMusicOn(){
		musicOnButton.SetActive(true);
		musicOffButton.SetActive(false);
		PlayerPrefs.SetInt("music", 1);
	}

	public void MakeMusicOff(){
		musicOnButton.SetActive(false);
		musicOffButton.SetActive(true);
		PlayerPrefs.SetInt("music", 0);
	}

	public void PauseGame(){
		filter.SetActive(true);
		playButton.SetActive(true);
		homeButton.SetActive(true);
		pauseButton.SetActive(false);
		FindObjectOfType<WorldController>().Pause();
		// On-Off Decisions
		if (PlayerPrefs.GetInt("sound") == 1) {
			soundOnButton.SetActive(true);
			soundOffButton.SetActive(false);
		} else {
			soundOffButton.SetActive(true);
			soundOnButton.SetActive(false);
		}
		if (PlayerPrefs.GetInt("music") == 1) {
			musicOnButton.SetActive(true);
			musicOffButton.SetActive(false);
		} else {
			musicOffButton.SetActive(true);
			musicOnButton.SetActive(false);
		}
	}

	public void ContinueGame(){
		RemoveMenuObjects();
		FindObjectOfType<WorldController>().Continue();
	}

	void RemoveMenuObjects(){
		filter.SetActive(false);
		playButton.SetActive(false);
		pauseButton.SetActive(true);
		homeButton.SetActive(false);
		soundOnButton.SetActive(false);
		soundOffButton.SetActive(false);
		musicOnButton.SetActive(false);
		musicOffButton.SetActive(false);
	}
}
