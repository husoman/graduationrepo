﻿using UnityEngine;
using System.Collections;
using Pathfinding;

// This script makes ufo chase us
// and makes ufo look at us all the time
// and lowers its health when it get hit
[RequireComponent (typeof(Seeker))]

public class EnemyAI : MonoBehaviour
{
	// What to chase
	Transform target;
	// How many times each second we will update our path
	float updateRate = 2f;
	// Caching
	Seeker seeker;
	Rigidbody2D rb;
	// The calculated path
	public Path path;
	// The AI's speed per second
	float speed = 800f;
	public ForceMode2D fMode;
	bool pathIsEnded = false;
	// The max distance from the AI to a waypoint for it to continue to the next waypoint
	float nextWaypointDistance = 3f;
	// The waypoint we are currently moving towards
	int currentWaypoint = 0;
	// Variables for facing right and left
	bool facingLeft;
	Vector3 scale;
	float horizontalSpeed;
	Transform healthBar;
	GameObject redLine;
	float posX, posY, posZ;

	void Start ()
	{
		target = FindObjectOfType<ManController> ().transform;
		facingLeft = true;
		//target = FindObjectOfType<ManController> ().transform;
		seeker = GetComponent<Seeker> ();
		rb = GetComponent<Rigidbody2D> ();
		healthBar = gameObject.transform.GetChild (0);
		redLine = GameObject.Find ("Block");
		if (target == null) {
			return;
		}
		// Start a new path to the target position, return the result to the OnPathComplete method
		seeker.StartPath (transform.position, target.position, OnPathComplete);
		StartCoroutine (UpdatePath ());
		posX = transform.position.x;
		posY = transform.position.y;
		posZ = transform.position.z;
	}

	IEnumerator UpdatePath ()
	{
		// Start a new path to the target position, return the result to the OnPathComplete method
		seeker.StartPath (transform.position, target.position, OnPathComplete);
		yield return new WaitForSeconds (1f / updateRate);
		StartCoroutine (UpdatePath ());
	}

	public void OnPathComplete (Path p)
	{
		if (!p.error) {
			path = p;
			currentWaypoint = 0;
		}
	}

	void Update ()
	{
		if (FindObjectOfType<WorldController> ().dragCompleted == true) {
			// Ufo hep aşağı düşüyor ?
			if (FindObjectOfType<WorldController> ().pause == false) {
				GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0f, 10f));		
			}
		} else {
			// Drag time, freeze the ufos
			transform.position = new Vector3 (posX, posY, posZ);
		}
		// Ufo flip mechanism
		horizontalSpeed = GetComponent<Rigidbody2D> ().velocity.x;
		if (horizontalSpeed < 0 && !facingLeft) {
			Flip ();
		} else if (horizontalSpeed > 0 && facingLeft) {
			Flip ();
		}
		if (healthBar.localScale.x == 0f) {
			Destroy (gameObject);
		}
		// When the ufo stays in previous level
		if (redLine.transform.position.x > transform.position.x) {
			Destroy (gameObject);
		}
	}

	void FixedUpdate ()
	{
		if (target == null) {
			return;
		}

		if (path == null) {
			return;
		}
		if (currentWaypoint >= path.vectorPath.Count) {
			if (pathIsEnded) {
				return;
			} else {
				//Debug.Log ("End of path reached");
				pathIsEnded = true;
				return;
			}
		}
		pathIsEnded = false;
		// Direction to the next waypoint
		Vector3 dir = (path.vectorPath [currentWaypoint] - transform.position).normalized;
		dir *= speed * Time.fixedDeltaTime;
		// Move the AI
		if (FindObjectOfType<WorldController> ().dragCompleted == true) {
			rb.AddForce (dir, fMode);	
		}
		float dist = Vector3.Distance (transform.position, path.vectorPath [currentWaypoint]);
		if (dist < nextWaypointDistance) {
			currentWaypoint++;
			return;
		}
	}

	void Flip ()
	{
		facingLeft = !facingLeft;
		scale = transform.localScale;
		scale.x *= -1;
		transform.localScale = scale;
	}

	void OnCollisionEnter2D (Collision2D coll)
	{
		if (coll.gameObject.tag == "Laser") {
			healthBar.localScale += new Vector3 (-1f, 0f, 0f);
		}
	}
}
