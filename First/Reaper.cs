﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reaper : MonoBehaviour {

	public GameObject curse;
	public bool declareWar;
	public AudioSource enchant;

	void Start () {
		declareWar = false;
	}

	void Update () {
		if (FindObjectOfType<WorldController> ().dragCompleted == true) {
			// Make the first attack
			if(declareWar == true){
				Instantiate (curse, new Vector2 (transform.position.x - 0.5f, transform.position.y + 0.5f), Quaternion.identity);
				GetComponent<Animator> ().Play ("reap", -1, 0f);
				if (PlayerPrefs.GetInt("sound") == 1) {
					enchant.Play();
				}
				StartCoroutine (Attack ());
				declareWar = false;
			}
		}
	}

	// Wait before the next attack
	IEnumerator Attack ()
	{
		yield return new WaitForSeconds (2f);
		Instantiate (curse, new Vector2 (transform.position.x - 0.5f, transform.position.y + 0.5f), Quaternion.identity);
		GetComponent<Animator> ().Play ("reap", -1, 0f);
		if (PlayerPrefs.GetInt("sound") == 1) {
			enchant.Play();
		}
		StartCoroutine (Attack ());
	}
}
