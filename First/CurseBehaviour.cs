﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Tries to hit the character with the force which is calculated in the first frame
public class CurseBehaviour : MonoBehaviour
{

	public Rigidbody2D rb;
	Transform characterTransform;
	float horizontalMultiplier = 25f;
	float verticalMultiplier = 30f;
	float initialPower = 500f;

	// Fire
	void Start ()
	{
		characterTransform = FindObjectOfType<ManController> ().transform;
		float positionDifferenceX = characterTransform.position.x - transform.position.x;
		float positionDifferenceY = characterTransform.position.y - transform.position.y;
		rb.AddForce (new Vector2 (positionDifferenceX * horizontalMultiplier, positionDifferenceY * verticalMultiplier + initialPower));
	}

	void Update ()
	{
		if (FindObjectOfType<WorldController> ().dragCompleted == false) {
			Destroy (rb.gameObject);
		}
	}

	void OnCollisionEnter2D (Collision2D coll)
	{
		Destroy (rb.gameObject);
	}
}
