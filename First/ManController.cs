﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// This script makes us jump and move sideways
// It deals with taking and dropping gun actions
public class ManController : MonoBehaviour
{
	float maxSpeed = 6f;
	bool facingRight = true;
	float move;
	public Animator anim;
	public bool grounded = false;
	public Transform groundCheck, cameraTransform;
	float groundRadius = 0.5f;
	public LayerMask whatIsGround;
	public AudioSource jumpSound;
	public bool gunActive = false;
	GameObject arm, cursor, gun;
	public Rigidbody2D rb;
	Text health;
	public GameObject healthBar;
	public int remainingHealth = 8;

	void Start ()
	{
		health = healthBar.GetComponent<Text> ();
		anim = GetComponent<Animator> ();
		arm = FindObjectOfType<ArmController> ().gameObject;
		arm.SetActive (false);
		rb = GetComponent<Rigidbody2D> ();
		cursor = FindObjectOfType<Cursor> ().gameObject;
		cursor.SetActive (false);
	}

	void FixedUpdate ()
	{
		if (FindObjectOfType<WorldController> ().pause == false) {
			grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround);
			anim.SetBool ("Ground", grounded);
			move = Input.GetAxis ("Horizontal");
			anim.SetFloat ("Speed", Mathf.Abs (move));
			// Flip operations
			if (FindObjectOfType<WorldController> ().dragCompleted == true) {
				anim.speed = 1f;
				rb.velocity = new Vector2 (move * maxSpeed, rb.velocity.y);	
				if (gunActive == true) {
					if ((Camera.main.ScreenToWorldPoint (Input.mousePosition).x > transform.position.x) && !facingRight) {
						Flip ();
					} else if ((Camera.main.ScreenToWorldPoint (Input.mousePosition).x < transform.position.x) && facingRight) {
						Flip ();
					}
				} else {
					if (move > 0 && !facingRight) {
						Flip ();
					} else if (move < 0 && facingRight) {
						Flip ();
					}	
				}
			} else {
				anim.speed = 0f;
			}
		}
	}

	void Update ()
	{
		health.text = remainingHealth.ToString ();
		// Jump
		if (grounded && (Input.GetKeyDown (KeyCode.W) || Input.GetKeyDown (KeyCode.UpArrow)) && FindObjectOfType<WorldController> ().dragCompleted == true) {
			if (FindObjectOfType<WorldController> ().pause == false) {
				transform.parent = null;
				rb.AddForce (new Vector2 (0f, 80f));
				if (PlayerPrefs.GetInt("sound") == 1) {
					jumpSound.Play ();
				}
			}
		}
		// When the character fall down
		if (cameraTransform.position.y - transform.position.y > 10f) {
			FindObjectOfType<WorldController> ().characterDead = true;
		}// "Gun" is a boolean variable regarding to animation changes
		anim.SetBool ("Gun", gunActive);
	}

	void Flip ()
	{
		facingRight = !facingRight;
		Vector3 scale = transform.localScale;
		scale.x *= -1;
		transform.localScale = scale;
	}

	void OnCollisionEnter2D (Collision2D coll)
	{	
		string colliderTag = coll.gameObject.tag;
		// When we hit an ufo
		if (colliderTag == "Ufo") {
			FindObjectOfType<WorldController> ().characterDead = true;
		}
		// When we touch a moving platform
		else if (colliderTag == "MovingPlatform") {
			transform.parent = coll.transform;
		}
		else if (colliderTag == "Temporary Block") {
			Destroy (coll.gameObject);
		}
		else if (colliderTag == "Curse") {
			FindObjectOfType<WorldController> ().characterDead = true;
		}
		else if(colliderTag == "Grass"){
			transform.parent = coll.transform;
		}
	}

	void OnTriggerEnter2D (Collider2D coll)
	{
		string colliderTag = coll.gameObject.tag;
		// When we hit a simple gun
		if (colliderTag == "Simple Gun") {
			TakeGun (coll.gameObject);
			FindObjectOfType<Weapon> ().fireRate = 0f;
		}
		// When we hit a automatic gun
		else if(colliderTag == "Automatic Gun"){
			TakeGun (coll.gameObject);
			FindObjectOfType<Weapon> ().fireRate = 10f;
			//GameObject simpleGun = GameObject.FindGameObjectWithTag ("Simple Gun");
			//simpleGun.SetActive (false);
		}
		else if (colliderTag == "Heart") {
			Destroy (coll.gameObject);
			remainingHealth++;
		}
		else if(colliderTag == "Spaceship"){
			SceneManager.LoadScene ("Second");
		}
		else if (colliderTag == "Bullet") {
			FindObjectOfType<WorldController> ().characterDead = true;
		}
	}

	// Handles general operations of gun taking
	void TakeGun(GameObject coll){
		coll.GetComponent<Collider2D> ().enabled = false;	// prevent colliding the same object multiple times
		if(gunActive == true){	// if we already have a gun
			Destroy(arm.transform.GetChild(1).gameObject);	// destroy it
		}
		coll.transform.GetChild (0).gameObject.SetActive (false);	// disable the little shine around the gun
		gunActive = true;	// a flag for related mechanism
		cursor.SetActive (true);
		arm.SetActive (true);
		// Re-scale the gun so it fits to arm
		Vector3 scale = coll.transform.localScale;
		scale.x = 0.5f;
		scale.y = 0.5f;
		coll.transform.localScale = scale;
		coll.transform.parent = arm.transform;	// make the gun child object of the arm
		coll.transform.localPosition = new Vector2 (0f, 0f);	// put it to right local position
		// And bring it to right rotation
		coll.transform.rotation = arm.transform.rotation;
		if(cursor.transform.position.x < transform.position.x){
			Quaternion rotation = coll.transform.localRotation;
			rotation.x = 180;
			coll.transform.localRotation = rotation;
		}
		UnityEngine.Cursor.visible = false;		// disable real mouse cursor
	}
}