﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{

	public Transform cameraTransform;
	public GameObject grass1, grass2, grass3, midgrounds, bigGrass, mediumGrass, boxes;
	public GameObject ufo, copyMan, reaper, turtle, turtle1;
	public GameObject levelObjects;
	List<Vector2> originalPositions = new List<Vector2>();
	int childrenNumber;

	void Start(){
		childrenNumber = levelObjects.transform.childCount;
		// Save original positions of level objects
		for (int i = 0; i < childrenNumber; i++) {
			// Get position of current child
			Vector2 position = new Vector2(levelObjects.transform.GetChild(i).localPosition.x, levelObjects.transform.GetChild(i).localPosition.y);
			// Add it to global list
			originalPositions.Add(position);
		}
	}

	// The main method of LevelController
	public void PrepareLevel(int levelCounter)
	{
		switch (levelCounter) {
			case -1:
				restartLevel_0();
				break;
			case 0:
				startLevel_0();
				break;
			case 1:
				startLevel_1();
				break;
			case 2:
				startLevel_2();
				break;
			case 3:
				startLevel_3();
				break;
			case 4:
				startLevel_4();
				break;
			case 5:
				startLevel_5();
				break;
			case 6:
				startLevel_6();
				break;
			case 7:
				startLevel_7();
				break;
			case 8:
				startLevel_8();
				break;
			case 9:
				startLevel_9();
				break;
			case 10:
				startLevel_10();
				break;
			default:
				startLastLevel();
				break;
		}
	}

	// Handles common routines for levels
	void levelStart()
	{
		resetPositions();
		// TASKS RELATED TO WORLD
		FindObjectOfType<WorldController>().inactivateOkayButton();
		FindObjectOfType<WorldController>().okayButton.SetActive(true);
		FindObjectOfType<WorldController>().activateDragFunctionality();
		FindObjectOfType<WorldController>().startYellow();
		FindObjectOfType<WorldController>().dragCompleted = false;	// Freeze the game, let the player move the platforms
		// TASKS RELATED TO MAıN CHARACTER
		FindObjectOfType<ManController>().anim.speed = 0f;
		FindObjectOfType<ManController>().transform.parent = null;
		FindObjectOfType<ManController>().rb.velocity = new Vector2(0f, 0f);	// For not falling with the speed of the previous level
	}

	// Tutorial level
	void startLevel_0()
	{
		Debug.Log("START LEVEL 0");
		FindObjectOfType<WorldController>().pause = true;
		FindObjectOfType<WorldController>().got.SetActive(true);
		FindObjectOfType<MenuController>().filter.SetActive(true);
		FindObjectOfType<MenuController>().pauseButton.SetActive(false);
		FindObjectOfType<WorldController>().tutorial = (GameObject)Instantiate(FindObjectOfType<WorldController>().tutorial, new Vector3(cameraTransform.position.x, cameraTransform.position.y, 1f), Quaternion.identity);
		FindObjectOfType<ManController>().anim.speed = 0f;
		FindObjectOfType<WorldController>().okayButton.SetActive(true);
		FindObjectOfType<WorldController>().inactivateOkayButton();
	}

	void restartLevel_0()
	{
		Debug.Log("RESTART LEVEL 0");
		SceneManager.LoadScene("First");
		FindObjectOfType<WorldController>().startYellow();
	}

	// Simple gun level
	void startLevel_1()
	{
		Debug.Log("START LEVEL 1");
		FindObjectOfType<WorldController>().characterLife.SetActive(true);
		levelStart();
	}

	// Ufo chase level
	void startLevel_2()
	{
		Debug.Log("START LEVEL 2");
		levelStart();
		FindObjectOfType<WorldController>().destroyUfos();
		Instantiate(ufo, new Vector2(cameraTransform.position.x + 7f, cameraTransform.position.y + 3.5f), Quaternion.identity);
		Instantiate(ufo, new Vector2(cameraTransform.position.x + 7f, cameraTransform.position.y + 4.5f), Quaternion.identity);
	}

	// Mirror character level
	void startLevel_3()
	{
		Debug.Log("START LEVEL 3");
		copyMan.SetActive(true);
		copyMan.transform.position = new Vector2(cameraTransform.position.x + FindObjectOfType<WorldController>().indexValueX, cameraTransform.position.y - FindObjectOfType<WorldController>().indexValueY);
		FindObjectOfType<ManController>().transform.parent = null;
		FindObjectOfType<CopyManController>().transform.parent = null;

	}

	// Bonus health level
	void startLevel_4()
	{
		Debug.Log("START LEVEL 4");
		copyMan.SetActive(false);
		FindObjectOfType<ManController>().transform.parent = null;
		levelStart();
	}

	// Reaper level
	void startLevel_5()
	{
		Debug.Log("START LEVEL 5");
		reaper.SetActive(false);
		reaper.SetActive(true);
		levelStart();
		FindObjectOfType<Reaper>().declareWar = true;
	}

	// Automatic gun level
	void startLevel_6()
	{
		Debug.Log("START LEVEL 6");
		reaper.SetActive(false);
		levelStart();
	}

	// Draggable moving platform
	void startLevel_7()
	{
		Debug.Log("START LEVEL 7");
		levelStart();
	}

	// Turtle level
	void startLevel_8()
	{
		Debug.Log("START LEVEL 8");
		levelStart();
	}

	// Magnet level
	void startLevel_9()
	{
		Debug.Log("START LEVEL 9");
	}

	// Green-dwarf level
	void startLevel_10()
	{
		Debug.Log("START LEVEL 10");

	}

	// Spaceship level
	void startLastLevel()
	{
		cameraTransform.position = new Vector3(0f, -20f, -10f);
		FindObjectOfType<ManController>().transform.position = new Vector2(-8.7f, -24.2f);
		FindObjectOfType<WorldController>().funbgm.Stop();
	}

	// Resets the positions of world objects
	void resetPositions(){
		for(int i = 0; i < childrenNumber; i++){
			levelObjects.transform.GetChild(i).localPosition = new Vector2 (originalPositions[i].x, originalPositions[i].y);
		}	
	}
}
