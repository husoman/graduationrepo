﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Makes gear movement with infinite number of platforms
public class Gear : MonoBehaviour
{
	public List<GameObject> platforms;
	List<float> radii;
	List<float> degrees;
	float counter = 0f;
	float posX;
	float posY;

	void Start ()
	{
		CalculateRadiiAndDegrees ();
	}

	void FixedUpdate ()
	{
		// Call turnPlatform() for each platform every delta-time
		for (int i = 0; i < platforms.Count; i++) {
			TurnPlatform (i, counter);
		}
		counter += Time.deltaTime / 2f; // delta-time is divided by 2 to slow the gears down
	}

	private void TurnPlatform (int index, float counter)
	{
		float degree = degrees [index];
		float radius = radii [index];
		platforms [index].transform.localPosition = new Vector2 (-1 * radius * Mathf.Cos (counter + degree), radius * Mathf.Sin (counter + degree));
	}

	private void CalculateRadiiAndDegrees ()
	{
		radii = new List<float> ();
		degrees = new List<float> ();
		for (int i = 0; i < platforms.Count; i++) {
			float x = platforms [i].transform.localPosition.x;
			float y = platforms [i].transform.localPosition.y;
			float radius = Mathf.Sqrt (x * x + y * y);
			degrees.Add (Mathf.Atan2 (y, x));
			radii.Add (radius);
		}
	}
}
