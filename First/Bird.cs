﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Makes the bird fly
// Makes it escape when it got hit
// Sends other birds message so that they can escape as well
public class Bird : MonoBehaviour, CommSubscriber
{

	bool hit = false;
	float speed = 3f;
	Animator anim;

	public enum State
	{
		FLYING,
		ESCAPING
	}

	public State ActiveState = State.FLYING;

	void Start ()
	{
		anim = GetComponent<Animator>();
	}

	void Update ()
	{
		if (FindObjectOfType<WorldController> ().dragCompleted == true) {
			transform.position = new Vector2 (transform.position.x + (Time.deltaTime * speed), transform.position.y);	
			anim.speed = 1f;
		} else {
			// Freeze birds in drag-drop time
			anim.speed = 0f;
		}
		switch (ActiveState) {
		case State.FLYING:
			// Flying operations
			speed = 3f;
			// State changing condition
			if (hit) {
				ActiveState = State.ESCAPING;
			}
			break;
		case State.ESCAPING:
			// Escaping operations
			//escape ();
			sendEscapeMessage ();	// So that other birds too will be aware of the danger
			break;
		default:
			Debug.Log ("Bird: Unknown state");
			break;
		}
	}

	void OnCollisionEnter2D (Collision2D coll)
	{
		if (coll.gameObject.tag == "Laser") {
			hit = true;
		}
	}

	// Method for turtle to get message
	public void receiveMessage (Message message)
	{
		switch (message.getContent ()) {
		case BirdMessage.MessageContent.BIRD_ESCAPE:
			escape ();
			break;
		default:
			Debug.Log ("ERROR: Message Content cannot be taken.");
			break;
		}
	}

	// Method for sending message
	private void sendEscapeMessage ()
	{
		BirdMessage message = new BirdMessage (Message.MessageContent.BIRD_ESCAPE);
		CommunicationController.getInstance ().sendMessage (message, Channel.Type.BIRD);
	}

	void escape ()
	{
		Vector3 scale = transform.localScale;
		scale.x = -1;
		transform.localScale = scale;
		speed = -4f;
	}
}
