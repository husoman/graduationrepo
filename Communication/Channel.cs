﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Channel
{
	// Store the types as enumeration
	public enum Type
	{
		TURTLE,
		BIRD,
		GREENDWARF
	}
	private Type type;
	// Create a list for subscribers
	List<CommSubscriber> subscribers = new List<CommSubscriber> ();

	// Constructor takes type and creates channel
	public Channel (Type type)
	{
		this.type = type;
	}

	// Method to reset the list of subscribers
	public void reset ()
	{
		subscribers = new List<CommSubscriber> ();
	}

	public void sendMessage (Message message)
	{
		foreach (CommSubscriber subscriber in subscribers) {	// for each subscriber in the list
			subscriber.receiveMessage (message);				// call the receiver method
		}
	}

	public void addSubscriber (CommSubscriber subscriber)
	{
		subscribers.Add (subscriber);	// add subscriber to the list
	}
}
