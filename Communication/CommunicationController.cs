﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommunicationController
{
	private static Dictionary <Channel.Type,Channel> channels = new Dictionary<Channel.Type,Channel> ();

	// Singleton Pattern
	private static CommunicationController instance = null;

	public static CommunicationController getInstance ()
	{
		if (instance == null) {
			instance = new CommunicationController ();
		}
		return instance;
	}

	// Constructor
	private CommunicationController ()
	{
		Debug.Log ("Communication Controller Constructed");	
	}

	public Channel createChannel (Channel.Type type)
	{
		Channel requestedChannel = null;
		if (channels.TryGetValue (type, out requestedChannel)) {	// search channel by type
			requestedChannel.reset ();	// reset the channel
		} else {
			requestedChannel = new Channel (type);	// if it doesn't exist create new channel
			channels.Add (type, requestedChannel);	// add it to the discitonary
		}
		return requestedChannel;
	}

	public void sendMessage (Message message, Channel.Type type)
	{
		Channel requestedChannel = null;
		if (channels.TryGetValue (type, out requestedChannel)) {	// search channel by type
			requestedChannel.sendMessage (message);	// send message
		} else {
			Debug.Log ("sendMessage else");
		}
	}
}

