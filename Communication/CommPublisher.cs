﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface CommPublisher
{

	void setChannel (Channel channel);

	void sendMessage (Message message, Channel channel);

}
