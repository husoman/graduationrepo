﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface CommSubscriber{

	void receiveMessage (Message message);

}
