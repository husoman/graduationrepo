﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Abstract class for messages
public abstract class Message
{
	public enum MessageContent
	{
		TURTLE_HIDE,
		BIRD_ESCAPE,
		ALERT_ALL_UNITS
	}

	protected MessageContent content;

	public abstract MessageContent getContent ();
}

class TurtleMessage : Message
{
	// Constructor for Turtle Message
	public TurtleMessage (MessageContent content)
	{
		this.content = content;
	}
	// Overriding abstract class method getContent
	public override MessageContent getContent ()
	{
		return content;
	}
}

class BirdMessage : Message
{
	public BirdMessage (MessageContent content)
	{
		this.content = content;
	}

	public override MessageContent getContent ()
	{
		return content;
	}
}

class GreenDwarfMessage : Message
{
	public GreenDwarfMessage (MessageContent content)
	{
		this.content = content;
	}

	public override MessageContent getContent()
	{
		return content;
	}
}
