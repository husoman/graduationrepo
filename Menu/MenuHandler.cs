﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuHandler : MonoBehaviour {

	public GameObject startButton, settingsButton, exitButton, backButton;
	public GameObject settingsTitle;
	public GameObject musicOnButton, musicOffButton, soundOnButton, soundOffButton;

	void Start(){
		PlayerPrefs.SetInt("sound", 1);
		PlayerPrefs.SetInt("music", 1);
	}

	// Starts the game
	public void StartGame(){
		SceneManager.LoadScene("First");
	}

	// Prepares the settings screen
	public void OpenSettings(){
		startButton.SetActive(false);
		settingsButton.SetActive(false);
		exitButton.SetActive(false);
		settingsTitle.SetActive(true);
		backButton.SetActive(true);
		if (PlayerPrefs.GetInt("sound") == 1) {
			soundOnButton.SetActive(true);
		} else {
			soundOffButton.SetActive(true);
		}
		if (PlayerPrefs.GetInt("music") == 1) {
			musicOnButton.SetActive(true);
		} else {
			musicOffButton.SetActive(true);
		}
	}

	// Kills the application
	public void ExitGame(){
		// No need to import a functionality here
		Debug.Log("Exit");
	}

	// Returns to main menu
	public void BackToMainMenu(){
		startButton.SetActive(true);
		settingsButton.SetActive(true);
		exitButton.SetActive(true);
		settingsTitle.SetActive(false);
		backButton.SetActive(false);
		musicOnButton.SetActive(false);
		musicOffButton.SetActive(false);
		soundOnButton.SetActive(false);
		soundOffButton.SetActive(false);
	}

	// Activates game music
	public void MakeMusicOn(){
		musicOnButton.SetActive(false);
		musicOffButton.SetActive(true);
		PlayerPrefs.SetInt("music", 0);
		Debug.Log("music: 0");
	}

	// Deactivates game music
	public void MakeMusicOff(){
		musicOffButton.SetActive(false);
		musicOnButton.SetActive(true);
		PlayerPrefs.SetInt("music", 1);
		Debug.Log("music: 1");
	}

	// Activates game sounds
	public void MakeSoundOn(){
		soundOnButton.SetActive(false);
		soundOffButton.SetActive(true);
		PlayerPrefs.SetInt("sound", 0);
		Debug.Log("sound: 0");
	}

	// Deactivates game sounds
	public void MakeSoundOff(){
		soundOffButton.SetActive(false);
		soundOnButton.SetActive(true);
		PlayerPrefs.SetInt("sound", 1);
		Debug.Log("sound: 1");
	}
}
