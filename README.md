# README #

### What is this repository for? ###

Design and Implementation of a Novel 2D Platform Game with Unity

### How do I get set up? ###

Online playable version is available at www.huseyintosun.net

### Contribution guidelines ###

You can examine the code by marked checkpoints. Contact me for more information.

### Who do I talk to? ###

Hüseyin Tosun, owner of the project.