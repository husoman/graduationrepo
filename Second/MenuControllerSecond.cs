﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuControllerSecond : MonoBehaviour {

	public GameObject homeButton, playButton, pauseButton, filter;
	public GameObject soundOnButton, soundOffButton;
	public GameObject musicOnButton, musicOffButton;

	void Start(){
		RemoveMenuObjects();
		pauseButton.SetActive(false);
	}

	public void ReturnToMainMenu(){
		SceneManager.LoadScene("Menu");
	}

	public void MakeSoundOn(){
		soundOffButton.SetActive(true);
		soundOnButton.SetActive(false);
		PlayerPrefs.SetInt("sound", 0);
	}

	public void MakeSoundOff(){
		soundOnButton.SetActive(true);
		soundOffButton.SetActive(false);
		PlayerPrefs.SetInt("sound", 1);
	}

	public void MakeMusicOn(){
		musicOnButton.SetActive(false);
		musicOffButton.SetActive(true);
		PlayerPrefs.SetInt("music", 0);
	}

	public void MakeMusicOff(){
		musicOnButton.SetActive(true);
		musicOffButton.SetActive(false);
		PlayerPrefs.SetInt("music", 1);
	}

	public void PauseGame(){
		filter.SetActive(true);
		playButton.SetActive(true);
		homeButton.SetActive(true);
		pauseButton.SetActive(false);
		FindObjectOfType<SpaceManager>().Pause();
		// On-Off Decisions
		if (PlayerPrefs.GetInt("sound") == 1) {
			soundOnButton.SetActive(true);
			soundOffButton.SetActive(false);
		} else {
			soundOffButton.SetActive(true);
			soundOnButton.SetActive(false);
		}
		if (PlayerPrefs.GetInt("music") == 1) {
			musicOnButton.SetActive(true);
			musicOffButton.SetActive(false);
		} else {
			musicOffButton.SetActive(true);
			musicOnButton.SetActive(false);
		}
	}

	public void ContinueGame(){
		RemoveMenuObjects();
		pauseButton.SetActive(true);
		FindObjectOfType<SpaceManager>().Continue();
	}

	void RemoveMenuObjects(){
		filter.SetActive(false);
		playButton.SetActive(false);
		homeButton.SetActive(false);
		soundOnButton.SetActive(false);
		soundOffButton.SetActive(false);
		musicOnButton.SetActive(false);
		musicOffButton.SetActive(false);
	}
}
