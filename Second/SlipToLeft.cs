﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Makes the rocks slip to left
public class SlipToLeft : MonoBehaviour
{
	public float speed = 0f;
	public Transform spaceBackground;

	void Update ()
	{
		if(!FindObjectOfType<SpaceManager>().paused){
			transform.localPosition = new Vector2 (transform.localPosition.x - speed, transform.localPosition.y);
			// when the object reaches to the end of the screen
			if (transform.position.x + transform.GetComponent<Renderer>().bounds.size.x < -1f * spaceBackground.GetComponent<Renderer> ().bounds.size.x / 2f) {
				speed = 0f;		// stop it
				transform.localPosition = new Vector2 (0f, transform.localPosition.y);	// put it to starting position
			}
		}
	}
}
