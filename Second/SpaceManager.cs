﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Cleans the old objects, prepares the new scene, controls the meteor flow
public class SpaceManager : MonoBehaviour
{
	public bool paused = false;
	public GameObject spaceShip, borders, life, youWin;
	bool scenePreparationCompleted;
	public AudioSource spaceMusic;
	public bool spaceFlow, gameFinishedSuccesfully;
	public Transform spaceBackgrounds, cameraTransform;
	int numberOfMeteors;

	void Start ()
	{
		life.SetActive (false);
		scenePreparationCompleted = false;
		spaceFlow = false;
		borders.SetActive (false);
		gameFinishedSuccesfully = false;
		StartCoroutine(WinTheGame());
		youWin.SetActive(false);
	}

	void Update ()
	{
		if (FindObjectOfType<Spaceship> ().spaceShipReady == true && scenePreparationCompleted == false) {
			prepareNewScene ();
			scenePreparationCompleted = true;
		}
		if (spaceFlow == true && !paused) {
			// Make the background flows back slowly
			spaceBackgrounds.position = new Vector2 (spaceBackgrounds.position.x - 0.01f, spaceBackgrounds.position.y);
			// Make the camera follow the space ship up & down smoothly
			cameraTransform.position = new Vector3 (0f, spaceShip.transform.position.y / 3f, -10f);
		}
	}

	public void Continue(){
		if(PlayerPrefs.GetInt("music") == 1){
			spaceMusic.Play();	
		}
		Time.timeScale = 1f;
		life.SetActive(true);
		paused = false;
	}

	public void Pause(){
		spaceMusic.Pause();
		Time.timeScale = 0f;
		life.SetActive(false);
		paused = true;
	}

	void prepareNewScene ()
	{
		// First clean-up the old objects
		var oldObjects = GameObject.FindGameObjectsWithTag ("Old World");
		foreach (GameObject item in oldObjects) {
			Destroy (item);
		}
		// Then bring the new ones to the scene
		spaceBackgrounds.position = new Vector2 (0f, 0f);
		spaceShip.transform.position = new Vector2 (-10f, 0f);
		spaceShip.transform.rotation = Quaternion.Euler (45, 0, -90);
		FindObjectOfType<Spaceship> ().mustGoStartingPos = true;
		StartCoroutine (FindObjectOfType<Spaceship> ().TakePosition ());
		if(PlayerPrefs.GetInt("music") == 1){
			spaceMusic.Play ();	
		}
		spaceFlow = true;
		// Procedural generation of meteors
		StartCoroutine(SendMeteor());
		StartCoroutine(SendPlanet());
		StartCoroutine(SendHurricane());
		// Some menu items are made active
		life.SetActive (true);
		FindObjectOfType<MenuControllerSecond>().pauseButton.SetActive(true);
	}

	// Instead of creating and destroying objects, use object pooling
	IEnumerator SendMeteor ()
	{
		yield return new WaitForSeconds (Random.Range(0f, 2f));
		var meteors = GameObject.FindGameObjectsWithTag ("Meteor");
		int randomNumber = Random.Range (0, meteors.Length);
		GameObject thisMeteor = meteors [randomNumber];		// choose one of the meteors randomly
		thisMeteor.GetComponent<SlipToLeft> ().speed = 0.1f;
		if (thisMeteor.transform.localPosition.x < 0f) {		// if it is already on the screen
			// do nothing
		} else { 
			float size = spaceBackgrounds.GetChild (0).GetComponent<Renderer> ().bounds.size.y;
			// set Y position randomly
			thisMeteor.transform.localPosition = new Vector2 (0f, Random.Range (-1f * size / 2f, size / 2f));
		}
		StartCoroutine (SendMeteor ());
	}

	// Method for sending planets to screen
	IEnumerator SendPlanet(){
		yield return new WaitForSeconds(Random.Range(10f, 20f));
		var planets = GameObject.FindGameObjectsWithTag("Planet");
		int randomNumber = Random.Range(0, planets.Length);
		GameObject thisPlanet = planets[randomNumber];	// choose random planet
		thisPlanet.GetComponent<SlipToLeft>().speed = 0.01f;
		if (thisPlanet.transform.localPosition.x < 0f) { // if already on the screen
			// do nothing
		} else {
			float size = spaceBackgrounds.GetChild(0).GetComponent<Renderer>().bounds.size.y;
			// set Y position randomly
			thisPlanet.transform.localPosition = new Vector2 (0f, Random.Range(-1f * size / 2f, size / 2f));
		}
		StartCoroutine(SendPlanet());
	}

	// Method for sending planets to screen
	IEnumerator SendHurricane(){
		yield return new WaitForSeconds(Random.Range(1f, 4f));
		var hurricanes = GameObject.FindGameObjectsWithTag("Hurricane");
		int randomNumber = Random.Range(0, hurricanes.Length);
		GameObject thisHurricane = hurricanes[randomNumber];	// choose random hurricane
		thisHurricane.GetComponent<SlipToLeft>().speed = 0.03f;
		if (thisHurricane.transform.localPosition.x < 0f) { // if already on the screen
			// do nothing
		} else {
			float size = spaceBackgrounds.GetChild(0).GetComponent<Renderer>().bounds.size.y;
			// set Y position randomly
			thisHurricane.transform.localPosition = new Vector2 (0f, Random.Range(-1f * size / 2f, size / 2f));
		}
		StartCoroutine(SendHurricane());
	}

	// After 2 minutes of survival, player wins the game!
	IEnumerator WinTheGame(){
		yield return new WaitForSeconds(120f);
		Debug.Log("You Have Won!");
		FindObjectOfType<FireworkStarter>().showTime = true;
		youWin.SetActive(true);
		FindObjectOfType<MenuControllerSecond>().pauseButton.SetActive(false);
		paused = true;
		FindObjectOfType<MenuControllerSecond>().filter.SetActive(true);
		life.SetActive(false);
		spaceShip.SetActive(false);
	}
}
