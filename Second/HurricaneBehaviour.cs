﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurricaneBehaviour : MonoBehaviour {

	public GameObject spaceship;
	float maxDistance = 5f;
	float minDistance = 1f;
	float maxStep = 5f;
	public float step = 1f;
	public bool deathComplete = false;

	void Update () {
		if (!FindObjectOfType<SpaceManager>().paused) {
			// Turn like a hurricane
			transform.Rotate(0, 0, -500f * Time.deltaTime);
			// Calculate distance between the hurricane and the spaceship
			float distance = Vector2.Distance(transform.position, spaceship.transform.position);
			if (distance < maxDistance) {
				// Attract the spaceship
				spaceship.transform.position = Vector2.MoveTowards(spaceship.transform.position, transform.position, Time.deltaTime * step);
				// Change the step value according to the distance (closer means more attraction)
				step = maxStep / distance;
			}
			// Flush death
			if(Vector2.Distance(transform.position, spaceship.transform.position) < minDistance){
				Debug.Log("FLUSH DEATH");
				// Turn off user control
				FindObjectOfType<Spaceship>().powerOff = false;
				// Flush effect
				spaceship.transform.parent = transform;
				Vector3 scale = spaceship.transform.localScale;
				scale.x -= 0.001f;
				scale.y -= 0.001f;
				spaceship.transform.localScale = scale;
				// Kill the spaceship 
				if (deathComplete == false){
					Debug.Log("DEATH COMPLETE FALSE");
					StartCoroutine(WaitBeforeDeath());
					deathComplete = true;
				}
			}
		}
	}

	IEnumerator WaitBeforeDeath ()
	{
		yield return new WaitForSeconds(1);
		Debug.Log("1 SEC WAıTED");
		FindObjectOfType<Spaceship>().HurricaneDeathOperations();
	}
}
