﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Makes the space background jump to right
public class SpaceContinuity : MonoBehaviour {

	public float sizeOfSpace;

	void Start(){
		sizeOfSpace = GetComponent<Renderer>().bounds.size.x;
	}

	void Update () {
		if(transform.position.x < -1f * sizeOfSpace){
			transform.position = new Vector2 (transform.position.x + (2f * sizeOfSpace), transform.position.y);
		}
	}
}
