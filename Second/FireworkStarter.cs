﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class enables fireworks at random times
public class FireworkStarter : MonoBehaviour
{
	public bool showTime = false;
	public GameObject[] fireworks = new GameObject[8];

	// First disable all fireworks
	void Start()
	{
		for (int i = 0; i < 8; i++) {
			fireworks[i].SetActive(false);
		}
	}

	// Then enable them after a random duration
	void Update(){
		if (showTime) {
			showTime = false;
			for (int i = 0; i < 8; i++) {
				StartCoroutine(WaitBeforeStarting(i));
			}
		}
	}

	IEnumerator WaitBeforeStarting(int fireworkCounter)
	{
		Debug.Log(fireworkCounter);
		yield return new WaitForSeconds(Random.Range(0f, 0.9f));
		fireworks[fireworkCounter].SetActive(true);
	}
}
