﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Makes the spaceship take off
// Deals with death and blinking
public class Spaceship : MonoBehaviour
{
	public bool spaceShipReady;
	public bool mustGoStartingPos;
	bool positionTaken;
	public bool powerOff;
	float speed = 5f;
	Rigidbody2D rb;
	public int remainingHealth = 3;
	public Text health;
	Animator explosion;
	public AudioSource explosionSound;
	float deathTime = 1.8f;
	Vector2 startingPosition = new Vector2(-6f, 0f);

	void Start ()
	{
		spaceShipReady = false;
		mustGoStartingPos = false;
		powerOff = true;
		rb = GetComponent<Rigidbody2D> ();
		explosion = transform.GetChild (0).GetComponent<Animator> ();
	}

	void FixedUpdate ()
	{
		if(!FindObjectOfType<SpaceManager>().paused){
			if (spaceShipReady == false) {	// Go up and be ready
				transform.position = new Vector2 (transform.position.x, transform.position.y + 2f * Time.deltaTime);
			}
			if (mustGoStartingPos == true && positionTaken == false) {
				transform.position = new Vector2 (transform.position.x + 2f * Time.deltaTime, transform.position.y);
			}
			if (powerOff == false) {
				rb.velocity = new Vector2 (Input.GetAxis ("Horizontal") * speed, Input.GetAxis ("Vertical") * speed);
			}
		}
	}

	void Update ()
	{
		if (transform.position.y >= 7f) {
			spaceShipReady = true;
		}
		health.text = remainingHealth.ToString ();
	}

	// For spaceship to take starting position
	public IEnumerator TakePosition ()
	{
		yield return new WaitForSeconds (2f);
		positionTaken = true;
		powerOff = false;
		FindObjectOfType<SpaceManager> ().borders.SetActive (true);
	}

	void MeteorDeathOperations ()
	{
		explosion.SetBool ("Damage", true);
		if (PlayerPrefs.GetInt("sound") == 1) {
			explosionSound.Play ();
		}
		GetComponent<Collider2D> ().enabled = false;
		if (remainingHealth > 1) {
			InvokeRepeating ("Blink", 0f, 0.2f);
			remainingHealth--;
			StartCoroutine (AfterDeath ());
		} else {
			GetComponent<Renderer> ().enabled = false;
			StartCoroutine (SmoothRestart ());
		}
	}

	public void HurricaneDeathOperations(){
		if(remainingHealth > 1){
			remainingHealth--;
			// Reset the position and parent value
			transform.position = startingPosition;
			transform.parent = null;
			// Fix the scale
			Vector3 scale = transform.localScale;
			scale.x = 0.5f;
			scale.y = 0.5f;
			transform.localScale = scale;
			// Fix the rotation
			transform.rotation = Quaternion.Euler (45f, 0f, -90f);
			FindObjectOfType<HurricaneBehaviour>().step = 1f;
		}else{
			SceneManager.LoadScene("Second");
		}
		FindObjectOfType<HurricaneBehaviour>().deathComplete = false;
	}

	void OnCollisionEnter2D (Collision2D coll)
	{
		if (coll.gameObject.tag == "Meteor") {	// if we hit a meteor
			MeteorDeathOperations();
		}
	}

	void Blink ()
	{
		if (GetComponent<Renderer> ().enabled == true) {
			GetComponent<Renderer> ().enabled = false;
		} else {
			GetComponent<Renderer> ().enabled = true;
		}
	}

	IEnumerator AfterDeath ()
	{
		yield return new WaitForSeconds (deathTime);
		explosion.SetBool ("Damage", false);
		GetComponent<Collider2D> ().enabled = true;
		CancelInvoke ("Blink");
		GetComponent<Renderer> ().enabled = true;
	}

	// For not restarting immediately (a better approach may be tried)
	IEnumerator SmoothRestart(){
		yield return new WaitForSeconds (deathTime);
		SceneManager.LoadScene ("Second");
	}
}
